const adminDao = require('../app/dao/admin');
const config = require('../config/default');
const jwt = require('jsonwebtoken');

exports.authCheck = function (req, res, next) {
	try {
		const auth = req.headers.authorization;
		if (!auth) {
			res.send({
				error: 1,
				messages: '最初にログインしてください'
			});
			return;
		}

		const jwt_token = auth.split(' ')[1];
		jwt.verify(jwt_token, config.jwtSecret, async function (err, data) {
			if (err || !data || !data.id) {
				res.send({
					error: 1,
					messages: '検証に失敗しました，最初にログインしてください'
				});
				return;
			}

			let user = await adminDao.getByConditions({
				id: data.id,
				token: jwt_token
			});
			if (!user) {
				res.send({
					error: 1,
					messages: 'ログインの有効期限が切れました，最初にログインしてください'
				});
				return;
			}

			req.userId = user.id;
			next();
		});
	} catch (e) {
		console.log(e)
		next(e);
	}
};