require ('dotenv').config();
const mysql = require('mysql');

module.exports = {
	apiPrefix: process.env.API_PREFIX,
	apiVersion: process.env.API_VERSION,
	awsAccessKeyId: process.env.AWS_ACCESS_KEY_ID,
	awsApiVersion: process.env.AWS_API_VERSION,
	awsSecretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
	awsS3Region: process.env.AWS_S3_REGION,
	awsBucketName: process.env.AWS_BUSCKET_NAME,
	smtpServer: process.env.SMTP_SERVER,
	smtpUsername: process.env.SMTP_USERNAME,
	smtpPassword: process.env.SMTP_PASSWORD,
	host: process.env.HOST,
	jwtSecret: process.env.JWT_SECRET,
	languageCode: process.env.LANGUAGE_CODE,
	port: process.env.PORT,
	stripeSecret: process.env.STRIPE_SECRET,
	urlPrefix: process.env.URL_PREFIX,
	version: process.env.VERSION
};