const knex = require('../dao');
const mysql = require('mysql');

const create = async function (user_id, company, phone, url) {
	let rows = await knex('projects')
		.insert({user_id: user_id});

	await knex('project_data')
		.insert([
			{
				project_id: rows[0],
				category_name: 'company_name',
				category_value: company
			},
			{
				project_id: rows[0],
				category_name: 'phone',
				category_value: phone
			},
			{
				project_id: rows[0],
				category_name: 'url',
				category_value: url
			}
		]);
	return rows[0];
};

const deleteIndustry = async function (user_id, industry_id) {
	let project = await knex('projects')
		.first('id')
		.where({user_id: user_id});
	return knex('project_industry')
		.delete()
		.where({
			project_id: project.id,
			industry_id: industry_id
		});
};

const get = async function (conditions) {
	let project = await knex('projects as p')
		.first([
			'p.id',
			'p.user_id',
			'p.avatar',
			'p.is_public',
			'p.is_approved',
			'p.created_at',
			'p.updated_at',
		])
		.where(conditions);
	if (!project) return null;
	project.attributes = await getMetaData(project.id);
	project.tags = await getTags(project.id);
	project.types = await getTypes(project.id);
	project.industries_tree = await getIndustryTree(project.id);
	project.industry_ids = project.industries_tree.map(each => each.id);
	project.member = await knex('users').first('id', 'avatar', 'name', 'position').where({id: project.user_id});

	delete project.user_id;
	return project;
};

const getCategoryNames = async function () {
	let rows = await knex('category_items')
		.select('name');
	return rows.map(e => e.name);
};

const getDataByCategoryName = function (project_id, category_name) {
	return knex('project_data')
		.first('*')
		.where({
			project_id: project_id,
			category_name: category_name
		});
};

const getById = function (id) {
	return get({id: id});
};

const getByUserId = function (user_id) {
	return get({user_id: user_id});
};

const getFavourites = async function (user_id, start, amount) {
	let project_ids = await knex('favourites')
		.select('project_id as id')
		.where({user_id: user_id})
		.orderBy('project_id');

	project_ids = project_ids.splice(start, amount);
	if (!project_ids.length) return [];
	return project_ids.map(e => e.id);
}

const getIndustries = async function () {
	let data = await knex('industries')
		.select('id', 'parent_id', 'label')
		.orderBy([
			{column: 'parent_id'},
			{column: 'id'}
		]);
	return flatToTree(data);
};

const getIndustryTree = async function (project_id) {
	return knex('project_industry')
		.select([
			'project_industry.industry_id as id',
			'i1.label as label1',
			'i2.label as label2',
			'i3.label as label3',
		])
		.join('industries as i3', 'i3.id', 'project_industry.industry_id')
		.join('industries as i2', 'i2.id', 'i3.parent_id')
		.join('industries as i1', 'i1.id', 'i2.parent_id')
		.where(knex.raw('project_industry.project_id = ?', project_id))
};

const getMetaData = async function (project_id) {
	let meta_data = {},
		raw_data = await knex('project_data')
			.select([
				'category_name',
				'category_value',
			])
			.where({project_id: project_id});

	raw_data.forEach(each => {
		meta_data[each.category_name] = each.category_value;
	});
	return meta_data;
};

const getTags = function (project_id) {
	return knex('project_tag')
		.select('tags.id', 'tags.label')
		.join('tags', 'tags.id', 'project_tag.tag_id')
		.where({project_id: project_id});
};

const getTypes = function (project_id) {
	return knex('project_type')
		.select('types.id', 'types.label')
		.join('types', 'types.id', 'project_type.type_id')
		.where({project_id: project_id});
};

const getIndustryTypes = async function (project_id) {
	let data = [],
		rows = await knex('types')
			.select()
			.orderBy('id');
	rows.forEach(row => {
		data.push({
			id: row.id,
			label: row.label,
			name: row.name,
			type: 'checkbox',
			value: false
		});
	});
	return data;
};

const update = function (data, conditions) {
	return knex('projects')
		.update(data)
		.where(conditions);
};

const search = async function (keywords, search_type, filters, industries, page, items_per_page, order_by) {
	let projects = [],
		offset = (parseInt(page, 10) - 1) * items_per_page,
		sql = 'SELECT p.id FROM `projects` p \n',
		total = 0;

		if (filters.length) {
			sql += mysql.format('INNER JOIN (\n' +
				'SELECT pt.project_id, t.label FROM `project_type` pt \n' +
				'JOIN `types` t ON t.id = pt.type_id \n' +
				'WHERE type_id IN (?)\n' +
				') ptt ON ptt.project_id = p.id\n', [filters]);
		}

		if (industries.length) {
			sql += mysql.format('INNER JOIN (\n' +
				'SELECT DISTINCT project_id FROM `project_industry` \n' +
				'INNER JOIN `industries` ON `industries`.parent_id IN (?)\n' +
				') piy ON piy.project_id = p.id\n', [industries]);
		}
		if (keywords.trim() !== '') {
			sql += mysql.format('INNER JOIN `project_data` pd ON pd.project_id = p.id\n' +
				'AND pd.category_value LIKE ?\n', ['%' + keywords.trim() + '%']);
		} else {
			sql += 'LEFT JOIN `project_data` pd ON pd.project_id = p.id\n';
		}
		sql += 'GROUP BY p.id\n';

	let rows = await knex.raw(sql);
	total = rows[0].length;

	if (total) {
		if (typeof order_by === 'string') {
			switch (order_by.trim().toLowerCase()) {
				case 'created':
				case 'created>':
					sql += 'ORDER BY p.created_at DESC\n';
					break;
				case 'updated':
				case 'updated>':
					sql += 'ORDER BY p.updated_at DESC\n';
					break;
				case 'created<':
					sql += 'ORDER BY p.created_at ASC\n';
					break;
				case 'updated<':
					sql += 'ORDER BY p.updated_at ASC\n';
					break;
			}
		}

		if (typeof order_by === 'object' && order_by.constructor === Array && order_by.length > 0) {
			let has_order_by = false;
			if (typeof order_by[0] === 'string') {
				switch (order_by[0].trim().toLowerCase()) {
					case 'created':
						sql += 'ORDER BY p.created_at ';
						has_order_by = true;
						break;
					case 'updated':
						has_order_by = true;
						sql += 'ORDER BY p.updated_at ';
						break;
				}
			}

			if (has_order_by && typeof order_by[1] === 'string') {
				if (order_by[1].trim().toLowerCase() === 'desc') {
					sql += 'DESC\n';
				} else {
					sql += 'ASC\n';
				}
			}
		}

		// Set pagination
		sql += 'LIMIT ' + offset + ', ' + items_per_page;
		rows = await knex.raw(sql);
		if (rows[0].length) {
			for (let i = 0; i < rows[0].length; i ++) {
				let project = await getById(rows[0][i].id);
				projects.push(project);
			}
		}
	}
	return {
		items: projects,
		total: total
	};
};

const updateType = async function (id, types) {
	let data = [],
		rows = await knex('project_data')
			.select('type_id')
			.where({project_id: id});

	if (rows.length) {
		rows = rows.map(e => e.type_id);
		types.filter(t => {
			if (rows.indexOf(t) === -1) data.push({
				project_id: id,
				type_id: t
			});
		});
	}
	return knex('project_data')
		.insert(data);
};

const updateData = async function (data, conditions) {
	let project = await knex('projects')
		.first('*')
		.where(conditions),
		category_names = await getCategoryNames();

	if (project) {
		let project_id = project.id;
		if (data.types && data.types.length) {
			await updateType(project.id, data.types);
			delete data.types;
		}

		if (Object.keys(data).length) {
			for (let key in data) {
				if (category_names.indexOf(key) > -1) {
					let category_data = await getDataByCategoryName(project_id, key);
					if (category_data) {
						await knex('project_data')
							.update({
								category_value: data[key]
							})
							.where({id: category_data.id});
					} else {
						await knex('project_data')
							.insert({
								project_id: project_id,
								category_name: category_name,
								category_value: data[key]
							});
					}
				}
				delete data[key];
			}

			if (Object.keys(data).length) {
				let attr_data = {};
				for (let key in data) {
					switch (key) {
						case 'avatar':
							if (data[key].trim() !== '') attr_data['avatar'] = data[key].trim();
							break;
						case 'is_approved':
						case 'is_public':
							if (parseInt(data[key]) === 0 || parseInt(data[key]) === 1) {
								attr_data[key] = parseInt(data[key]);
							}
							break;

					}
				}
				if (Object.keys(attr_data).length) {
					await knex('projects')
						.update(attr_data)
						.where({id: project_id});
				}
			}
		}
	}
};

module.exports = {
	create: create,
	deleteIndustry: deleteIndustry,
	get: get,
	getByUserId: getByUserId,
	getFavourites: getFavourites,
	getIndustries: getIndustries,
	getIndustryTypes: getIndustryTypes,
	getTypes: getTypes,
	search: search,
	update: update,
	updateData: updateData,
}

function flatToTree (items, id = 0, link = 'parent_id') {
	return items.filter(item => item[link] === id)
		.map(item => ({ ...item, children: flatToTree(items, item.id) }));
}