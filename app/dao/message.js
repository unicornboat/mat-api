const knex = require('../dao');

const add = function (sender_id, receiver_id, project_id, content, time) {
	return knex('messages')
		.insert({
			sender_id: sender_id,
			receiver_id: receiver_id,
			project_id: project_id,
			content: content,
			created_at_iso: time
		})
};

const getList = async function (user_id) {
	let list = [],
		rows = await knex('messages')
			.select('project_id')
			.where({sender_id: user_id})
			.orWhere({receiver_id: user_id})
			.groupBy('project_id')
			.orderBy('created_at', 'desc');

	for (let i = 0; i < rows.length; i ++) {
		let message = await knex('messages as m')
			.select([
				'm.id',
				'm.sender_id',
				'm.receiver_id',
				'm.project_id',
				'm.is_read',
				'm.content',
				'u.name as name',
				'u.avatar as avatar',
				'm.created_at_iso'
			])
			.join('users as u', 'u.id', 'm.receiver_id')
			.where({receiver_id: user_id})
			.andWhere({project_id: rows[i].project_id})
			.orderBy('m.created_at', 'desc')
			.first();
		if (!message) {
			message = await knex('messages as m')
				.select([
					'm.id',
					'm.sender_id',
					'm.receiver_id',
					'm.project_id',
					'm.is_read',
					'm.content',
					'u.name as name',
					'u.avatar as avatar',
					'm.created_at_iso'
				])
				.join('users as u', 'u.id', 'm.receiver_id')
				.where({sender_id: user_id})
				.andWhere({project_id: rows[i].project_id})
				.orderBy('m.created_at', 'desc')
				.first();
		}

		let project = await getProject(message.sender_id === user_id ? message.receiver_id : message.sender_id);
		message.company_name = project.company_name;
		message.company_avatar = project.company_avatar;
		// message.project_id = project.id;
		list.push(message);
	}
	return list;
};

const getMessages = async function (id1, id2) {
	let messages = await getMessagesByConditions({
			receiver_id: id2,
			sender_id: id1
		});
	messages = messages.concat(await getMessagesByConditions({
		receiver_id: id1,
		sender_id: id2
	}));
	// messages.sort((a, b) => a.created_at - b.created_at);
	messages.sort((a, b) => {
		return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
	});
	// console.log(messages)
	return messages;
};

const getMessagesByReceiver = function (receiver_id) {
	return getMessagesByConditions({receiver_id: receiver_id});
};

const getMessagesBySender = function (sender_id) {
	return getMessagesByConditions({sender_id: sender_id});
};

const getMessagesByConditions = function (conditions) {
	return knex('messages')
		.select([
			'messages.id',
			'company.category_value as company_name',
			'projects.avatar as company_avatar',
			'messages.sender_id',
			'sender.name as sender_name',
			'sender.avatar as sender_avatar',
			'messages.receiver_id',
			'messages.project_id',
			'messages.is_read',
			'messages.content',
			'receiver.name as receiver_name',
			'receiver.avatar as receiver_avatar',
			'messages.created_at_iso as created_at'
		])
		.where(conditions)
		.join('users as sender', 'sender.id', 'messages.sender_id')
		.join('users as receiver', 'receiver.id', 'messages.receiver_id')
		.leftJoin('project_data as company', function () {
			this.on('company.project_id', '=', 'messages.project_id')
			this.andOn(knex.raw('company.category_name = "company_name"'))
		})
		.leftJoin('projects', 'projects.id', 'messages.project_id');
		// .orderBy('messages.created_at', 'desc');
};

const getProject = async function (user_id) {
	let project = await knex('projects')
		.first([
			'projects.id',
			'projects.avatar',
			'project_data.category_value as company_name'
		])
		.leftJoin('project_data', function () {
			this.on('project_data.project_id', '=', 'projects.id')
				.andOn('project_data.category_name', '=', knex.raw('?', ['company_name']))
		})
		.where({user_id: user_id});
	if (!project) return null;
	return project;
};

const markAsRead = function (receiver_id, sender_id) {
	return knex('messages')
		.update({is_read: 1})
		.where({
			receiver_id: receiver_id,
			sender_id: sender_id
		});
};

module.exports = {
	add: add,
	getList: getList,
	getMessages: getMessages,
	markAsRead: markAsRead,
}