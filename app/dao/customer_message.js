const knex = require('../dao');

const add = function (name, company_name, email, content) {
	return knex('customer_messages')
		.insert({
			name: name,
			company_name: company_name,
			email: email,
			content: content
		})
		// .onConflict(['user_id', 'project_id'])
		// .ignore();
};

module.exports = {
	add: add,
}