const config = require('../../config/default');
const knex = require('../dao');
const jwt = require('jsonwebtoken');

const getByHash = function (hash) {
	return knex('jwt_tokens')
		.first('*')
		.where({token: hash})
};

const remove = function (user_id) {
	return knex('jwt_tokens')
		.delete()
		.where({user_id: user_id ? user_id : null})
};

const update = async function (user_id, longterm) {
	let jwt_token = jwt.sign({id: user_id}, config.jwtSecret);

	await knex('jwt_tokens')
		.delete()
		.where({user_id: user_id});

	await knex('jwt_tokens')
		.insert({
			user_id: user_id,
			token: jwt_token,
			life: longterm ? 2592000 : 1800
		})
	return jwt_token;
};

module.exports = {
	getByHash: getByHash,
	remove: remove,
	update: update,
}