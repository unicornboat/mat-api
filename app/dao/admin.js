const knex = require('../dao');

const getByConditions = function (conditions) {
	return knex('admin')
		.first('id', 'username', 'name', 'avatar')
		.where(conditions)
		// .on('start', function (db) {
		// 	console.log(db.toString())
		// })
}

const update = function (data, conditions) {
	return knex('admin')
		.update(data)
		.where(conditions)
};

module.exports = {
	getByConditions: getByConditions,
	update: update,
}