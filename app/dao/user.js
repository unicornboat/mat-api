const crypto = require('crypto');
const configDao = require('./config');
const knex = require('../dao');
const language = require('../../lib/language');
const mailer = require('../../lib/mailer');
const projectDao = require('./project');

const addToFavourites = async function (user_id, project_id) {
	let user = await get(user_id);
	if (user && user.project.id !== project_id) {
		let is_added = await isProjectInFavourites(user_id, project_id);
		if (!is_added) {
			await knex('favourites')
				.insert({
					user_id: user_id,
					project_id: project_id
				});
		}
	}
};

const create = async function (email, name, password, company, phone, url) {
	let email_token = crypto.randomBytes(64).toString('hex'),
		password_hash = crypto.createHash('sha256').update(password).digest('hex');

	let user_rows = await knex('users')
		.insert({
			company: company,
			email: email,
			email_token: email_token,
			name: name,
			password: password_hash,
			phone: phone,
			url: url
		});

	await projectDao.create(user_rows[0], company, phone, url);
	return await get(user_rows[0]);
};

const generatePasswordToken = async function (email) {
	let password_token = crypto.randomBytes(16).toString('hex'),
		user = await getByConditions({email: email});

	if (!user) throw new Error(language('Cannot find user'));
	await update({
		password_token: password_token
	}, {id: user.id});

	return password_token;
};

const get = function (id, full_data = false) {
	return getByConditions({id: id}, full_data);
};

const getByConditions = async function (conditions, full_data = false) {
	let fields = [
		'user.id',
		'user.avatar',
		'user.email',
		'user.name',
		'user.position',
		'user.duty',
		'user.introduction',
		'user.is_approved',
		'user.is_email_verified',
		'user.is_phone_verified',
		'user.is_public',
		'user.status',
		'user.tickets',
		'user.introduction',
		'user.created_at',
		'user.updated_at',
	];

	if (full_data) {
		fields.push('user.company');
		fields.push('user.email_token');
	}

	let user = await knex('users as user')
		.first(fields)
		.where(conditions);
	if (!user) return null;

	let favourites = await knex('favourites')
		.select('project_id as id')
		.where({user_id: user.id});
	user.favourites = favourites.map(e => e.id);

	user.project = await projectDao.getByUserId(user.id);
	return user;
}

const isEmailExist = async function (email) {
	let rows = await knex('users')
		.select('id')
		.where({email: email});
	return rows.length > 0;
};

const isProjectInFavourites = async function (user_id, project_id) {
	let rows = await knex('favourites')
		.where({
			user_id: user_id,
			project_id: project_id
		});
	return rows.length > 0;
};

const removeFromFavourites = function (user_id, project_id) {
	return knex('favourites')
		.delete()
		.where({
			user_id: user_id,
			project_id: project_id
		});
};

const togglePublicity = async function (id) {
	let user = await knex('users')
		.first('*')
		.where({id: id});
	if (user) {
		await knex('users')
			.update({is_public: user.is_public === 1 ? 0 : 1})
			.where({id: id});
	}
};

const update = function (data, conditions) {
	return knex('users')
		.update(data)
		.where(conditions);
};

module.exports = {
	addToFavourites: addToFavourites,
	create: create,
	generatePasswordToken: generatePasswordToken,
	get: get,
	getByConditions: getByConditions,
	isEmailExist: isEmailExist,
	removeFromFavourites: removeFromFavourites,
	togglePublicity: togglePublicity,
	update: update,
}