const knex = require('../dao');

const get = async function (key) {
	let row = await knex('core_config')
		.first('*')
		.where({key: key});
	return row ? row['value'] : null;
};

module.exports = {
	get: get,
}