const {mysql, pool} = require('../../config/db');
const app = {};
const httpError = require('../../lib/http-error');
const language = require('../../lib/language');
const validator = require('validator');
const tokenDao = require('../dao/token');

app.isValid = function (token) {
	return new Promise(async (resolve, reject) => {
		try {
			let record = await tokenDao.getByHash(token);
			resolve(record);
		} catch (err) {
			console.log(err);
			reject(err);
		}
	});
};

module.exports = app;