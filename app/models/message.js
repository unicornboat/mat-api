const {mysql, pool} = require('../../config/db');
const app = {};
const httpError = require('../../lib/http-error');
const language = require('../../lib/language');
const moment = require('moment');
const project  = require('./project');
const validator = require('validator');

app.add = function (sender_id, receiver_id, content) {
	return new Promise((resolve, reject) => {
		if (!validator.isLength(content.trim(), {max: 500, min: 1})) {
			reject(new httpError(400, language('Content cannot be shorter than %s or longer than %s', 1, 500)));
			return;
		}

		let sql = mysql.format('INSERT INTO `messages` SET ?', {
				sender_id: sender_id,
				receiver_id: receiver_id,
				content: content,
				created_at: moment().format('YYYY-MM-DD HH:mm:ss')
			});
		pool.query(sql, err => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}
			getMessages(sender_id, receiver_id)
				.then(data => {
					resolve(data);
				})
				.catch(err => {
					reject(err);
				});
		});
	});
};

app.getList = function (user_id) {
	let data = [];

	return new Promise((resolve, reject) => {
		pool.query('SELECT * FROM `messages` ms WHERE ms.`sender_id` = ?', [user_id], function (err, rows) {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}

			pool.query('SELECT * FROM `messages` m ' +
				'JOIN `users` u ON u.id = m.sender_id ' +
				'WHERE m.sender_id = ?', [user_id], function (err, rows) {
				if (err) {
					reject(new httpError(500, err.message));
					return;
				}
			});
		});
	});
	let sql = mysql.format('SELECT * FROM (\n' +
		'SELECT *\n' +
		'FROM (\n' +
		'\tSELECT m.id, m.content, m.is_read, m.created_at, s.id sender_id, ? receiver_id,\n' +
		'\t\tp.id project_id, pd.category_value company_name,\n' +
		'\t\tp.avatar project_avatar, s.avatar sender_avatar, r.avatar receiver_avatar\n' +
		'\tFROM `messages` m\n' +
		'JOIN `users` s ON s.id = m.sender_id\n' +
		'JOIN `projects` p ON p.user_id = s.id\n' +
		'JOIN `users` r ON r.id = receiver_id AND r.id = ?\n' +
		'LEFT JOIN `project_data` pd\n' +
		'\t\tON pd.project_id = m.project_id AND pd.category_name = "company_name"\n' +
		'\t\tGROUP BY p.id ORDER BY m.created_at DESC\n' +
		'\t) m1\n' +
		'UNION ALL\n' +
		'SELECT *\n' +
		'FROM (\n' +
		'\tSELECT m.id,  m.content,  m.is_read,  m.created_at, ? sender_id, r.id receiver_id,\n' +
		'\t\tp.id project_id,  pd.category_value company_name,\n' +
		'\t\tp.avatar project_avatar, s.avatar sender_avatar, r.avatar receiver_avatar\n' +
		'\tFROM `messages` m\n' +
		'JOIN `users` s ON s.id = m.sender_id AND s.id = ?\n' +
		'JOIN `projects` p ON p.user_id = s.id\n' +
		'JOIN `users` r ON r.id = receiver_id\n' +
		'LEFT JOIN `project_data` pd\n' +
		'\t\tON pd.project_id = m.project_id AND pd.category_name = "company_name"\n' +
		'\tGROUP BY p.id ORDER BY m.created_at DESC\n' +
		'\t) m2\n' +
		') m_all\n' +
		'ORDER BY m_all.created_at DESC', [receiver_id, receiver_id, receiver_id, receiver_id]);
	return new Promise((resolve, reject) => {
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}

			if (!rows.length) {
				resolve([]);
				return;
			}

			let id_arr = [],
			data = [];
			rows.forEach(row => {
				let ids1 = row.sender_id.toString() + ',' + row.receiver_id.toString(),
					ids2 = row.receiver_id.toString() + ',' + row.sender_id.toString();

				if (id_arr.indexOf(ids1) === -1 && id_arr.indexOf(ids2) === -1) {
					id_arr.push(ids1);
					id_arr.push(ids2);
					data.push(row);
				}
			});
			resolve(data);
		});
	});
};

app.get = function (id1, id2) {
	return new Promise((resolve, reject) => {
		setToRead(id1, id2)
			.then(() => {
				getMessages(id1, id2)
					.then(data => {
						resolve(data);
					})
					.catch(err => {
						reject(err);
					});
			})
			.catch(err => {
				reject(err);
			});
	});
};

module.exports = app;

function getMessages (id1, id2) {
	return new Promise((resolve, reject) => {
		let sql = mysql.format('SELECT m.*, s.avatar sender_avatar \n' +
			'FROM `messages` m \n' +
			'JOIN `users` s ON s.id = m.sender_id \n' +
			'WHERE (receiver_id = ? AND sender_id = ?)\n' +
			'\tOR (sender_id = ? AND receiver_id = ?)\n' +
			'ORDER BY m.created_at ASC\n', [id1, id2, id1, id2]);
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}
			resolve(rows);
		});
	});
}

function setToRead (id1, id2) {
	let sql = mysql.format('SELECT * FROM `messages` \n' +
		'WHERE (\n' +
		'\t(receiver_id = ? AND sender_id = ?) OR (sender_id = ? AND receiver_id = ?)\n' +
		') AND is_read = 0', [id1, id2, id1, id2]);
	return new Promise((resolve, reject) => {
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}

			if (!rows.length) {
				resolve();
				return;
			}

			sql = mysql.format('UPDATE `messages` SET is_read = 1\n' +
				'WHERE id IN (?)', [rows.map(r => r.id)]);
			pool.query(sql, err => {
				if (err) {
					reject(new httpError(500, err.message));
					return;
				}
				resolve();
			});
		});
	});
}

function getListBySender () {
	let sql = 'SELECT m.id, \n' +
		'm.sender_id, s.name sender_name, s.avatar sender_avatar, \n' +
		'm.receiver_id, r.name receiver_name, r.avatar receiver_avatar, \n' +
		'm.content, m.created_at FROM `messages` m \n' +
		'JOIN `users` s ON s.id = m.sender_id\n' +
		'JOIN `users` r ON r.id = m.receiver_id\n' +
		'WHERE m.sender_id = 7\n' +
		'ORDER BY m.created_at DESC';
}