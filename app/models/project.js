const {mysql, pool} = require('../../config/db');
const config = require('../../config/default');
const app = {};
const httpError = require('../../lib/http-error');
const language = require('../../lib/language');

app.create = function (user_id, company_name, phone, url) {
	return new Promise((resolve, reject) => {
		let sql = mysql.format('INSERT INTO `projects` (user_id) ' +
				'SELECT * FROM (SELECT ?) AS t1 ' +
				'WHERE NOT EXISTS (' +
					'SELECT user_id FROM `projects` WHERE user_id = ?' +
			') LIMIT 1', [user_id, user_id]);
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			let project_id = rows.insertId;

			let sql = mysql.format('INSERT INTO `project_data` (project_id, category_name, category_value) ' +
				'VALUES (?, ?, ?), (?, ?, ?), (?, ?, ?)',
				[
					project_id, 'company_name', company_name,
					project_id, 'phone', phone,
					project_id, 'url', url
				]);
			pool.query(sql, (err, rows) => {
				if (err) {
					reject(new httpError(500, err));
					return;
				}

				getOne('id', project_id).then(data => {
					resolve(data);
				}).catch(err => {
					reject(err);
				});
			});
		});
	});
};

app.getFavourites = function (id_arr, offset, amount) {
	return new Promise((resolve, reject) => {
		let sql = mysql.format('SELECT * FROM `projects` WHERE id IN ? LIMIT ?, ? ORDER BY id', [id_arr, offset, amount]);
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
		});
	});
};

app.get = getOne;
app.getMany = getMany;

app.search = function (keywords, search_type, filters, industries, page, items_per_page, order_by) {
	let data = {
			items: [],
			total: 0
		},
		offset = (parseInt(page, 10) - 1) * items_per_page;
	return new Promise((resolve, reject) => {
		let sql = 'SELECT p.id FROM `projects` p \n';

		if (filters.length) {
			sql += mysql.format('INNER JOIN (\n' +
				'SELECT pt.project_id, t.label FROM `project_type` pt \n' +
				'JOIN `types` t ON t.id = pt.type_id \n' +
				'WHERE type_id IN (?)\n' +
			') ptt ON ptt.project_id = p.id\n', [filters]);
		}

		if (industries.length) {
			sql += mysql.format('INNER JOIN (\n' +
				'SELECT DISTINCT project_id FROM `project_industry` \n' +
				'INNER JOIN `industries` ON `industries`.parent_id IN (?)\n' +
			') piy ON piy.project_id = p.id\n', [industries]);
		}
		if (keywords.trim() !== '') {
			sql += mysql.format('INNER JOIN `project_data` pd ON pd.project_id = p.id\n' +
				'AND pd.category_name = "company_name"\n' +
				'AND pd.category_value LIKE ?\n', ['%' + keywords.trim() + '%']);
		} else {
			sql += 'LEFT JOIN `project_data` pd ON pd.project_id = p.id\n';
		}
		sql += 'GROUP BY p.id\n';
		// console.log(sql);

		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}

			if (!rows.length) {
				resolve(data);
				return;
			}

			// Update total count
			data.total = rows.length;

			// Order by given in string
			if (typeof order_by === 'string') {
				switch (order_by.trim().toLowerCase()) {
					case 'created':
					case 'created>':
						sql += 'ORDER BY p.created_at DESC\n';
						break;
					case 'updated':
					case 'updated>':
						sql += 'ORDER BY p.updated_at DESC\n';
						break;
					case 'created<':
						sql += 'ORDER BY p.created_at ASC\n';
						break;
					case 'updated<':
						sql += 'ORDER BY p.updated_at ASC\n';
						break;
				}
			}

			// Order by give in array
			if (typeof order_by === 'object' && order_by.constructor === Array && order_by.length > 0) {
				let has_order_by = false;
				if (typeof order_by[0] === 'string') {
					switch (order_by[0].trim().toLowerCase()) {
						case 'created':
							sql += 'ORDER BY p.created_at ';
							has_order_by = true;
							break;
						case 'updated':
							has_order_by = true;
							sql += 'ORDER BY p.updated_at ';
							break;
					}
				}

				if (has_order_by && typeof order_by[1] === 'string') {
					if (order_by[1].trim().toLowerCase() === 'desc') {
						sql += 'DESC\n';
					} else {
						sql += 'ASC\n';
					}
				}
			}

			// Set pagination
			sql += mysql.format('LIMIT ?, ?', [offset, items_per_page]);
			// console.log(sql);

			// Fetch the limited projects
			pool.query(sql, (err, rows) => {
				if (err) {
					reject(new httpError(500, err.message));
					return;
				}

				if (!rows.length) {
					resolve([]);
					return;
				}

				// Get detailed data
				loopMany(rows.map(m => m.id), items => {
					data.items = items;
					resolve(data);
				});
			});
		});
	});
};

app.getById = function (id, user_id) {
	return new Promise((resolve, reject) => {
		getOne({'id': id}).then(data => {
			if (!user_id) {
				resolve(data);
				return;
			}
			resolve(data);
		}).catch(err => {
			reject(err);
		});
	});
};

app.getByUserId = function (user_id) {
	return new Promise((resolve, reject) => {
		getOne('user_id', user_id).then(data => {
			resolve(data);
		}).catch(err => {
			reject(err);
		});
	});
};

app.setPublic = function (id, val) {
	return new Promise(function (resolve, reject) {
		if (isNaN(val) || (parseInt(val) !== 1 && parseInt(val) !== 0)) {
			reject(new httpError(400, 'Invalid parameter. setPublic expects "val" to be 1 or 0'));
			return;
		}
		pool.query('UPDATE `projects` SET is_public = ? WHERE id = ?', [val, id], function (err, rows) {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}
			resolve(rows.changedRows);
		});
	});
}

app.getTypes = function () {
	return new Promise((resolve, reject) => {
		pool.query('SELECT * FROM `types` ORDER BY id ASC', (err, rows) => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}
			let data = [];
			rows.forEach(row => {
				data.push({
					id: row.id,
					label: row.label,
					name: row.name,
					type: 'checkbox',
					value: false
				});
			});
			resolve(data);
		});
	});
};

app.updateIndustries = function (n_project_id, a_ids) {
	return new Promise((resolve, reject) => {
		pool.query('DELETE FROM `project_industry` WHERE project_id = ?', [n_project_id], err => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}

			let data = a_ids.map(n_industry_id => {
				return [n_project_id, n_industry_id];
			});
			pool.query('INSERT INTO `project_industry` (project_id, industry_id) VALUES ?', [data], err => {
				if (err) {
					console.log(err);
					reject(new httpError(500, err.message));
					return;
				}
				pool.query('SELECT piy.industry_id id,\n' +
						'iy1.label label1, iy2.label label2, iy3.label label3\n' +
					'FROM `project_industry` piy\n' +
					'JOIN `industries` iy3 ON iy3.id = piy.industry_id\n' +
					'JOIN `industries` iy2 ON iy2.id = iy3.parent_id\n' +
					'JOIN `industries` iy1 ON iy1.id = iy2.parent_id\n' +
					'WHERE piy.project_id = ?', [n_project_id], (err, rows) => {
					if (err) {
						console.log(err);
						reject(new httpError(500, err.message));
						return;
					}
					resolve(rows);
				});
			});
		});
	});
}

app.deleteIndustry = function (n_project_id, n_industry_id) {
	return new Promise((resolve, reject) => {
		pool.query('DELETE FROM `project_industry` WHERE project_id = ? AND industry_id = ?', [n_project_id, n_industry_id], err => {
			if (err) {
				console.log(err);
				reject(new httpError(500, err.message));
				return;
			}
			pool.query('SELECT piy.industry_id id,\n' +
					'iy1.label label1, iy2.label label2, iy3.label label3\n' +
				'FROM `project_industry` piy\n' +
				'JOIN `industries` iy3 ON iy3.id = piy.industry_id\n' +
				'JOIN `industries` iy2 ON iy2.id = iy3.parent_id\n' +
				'JOIN `industries` iy1 ON iy1.id = iy2.parent_id\n' +
				'WHERE piy.project_id = ?', [n_project_id], (err, rows) => {
				if (err) {
					console.log(err);
					reject(new httpError(500, err.message));
					return;
				}
				// console.log(rows)
				resolve(rows);
			});
		});
	});
}

app.getIndustries = getIndustries;
app.update = update;
app.updateAvatar = updateAvatar;

module.exports = app;

function loopMany (id_arr, callback = function () {}, results = []) {
	if (!id_arr.length) {
		callback(results);
		return;
	}

	getOne({id: id_arr.splice(0, 1)[0]})
		.then(data => {
			results.push(data);
			loopMany(id_arr, callback, results);
		})
		.catch(err => {
			console.log('loopMany', err);
			return results;
		});
}

function flatToTree (items, id = 0, link = 'parent_id') {
	return items.filter(item => item[link] === id)
		.map(item => ({ ...item, children: flatToTree(items, item.id) }));
}

function appendConditions (sql, conditions) {
	let keys = [], values = [];
	if (conditions) {
		sql += ' WHERE ';
		for (let key in conditions) {
			keys.push(key + ' = ?');
			values.push(conditions[key]);
		}
		sql += keys.join(' AND ');
		sql = mysql.format(sql, values);
	}
	return sql;
}

function getMany (conditions, page = 1, amount = 10) {
	return new Promise((resolve, reject) => {
		let amount = 10,
			and = [],
			or = [],
			page = 1,
			sql = 'SELECT * FROM `projects`';

		if (conditions) {
			if (typeof conditions['and'] === 'object') {
				if (
					conditions['and'].constructor === Array
					&& conditions['and'].length === 2
					&& typeof conditions['and'][0] === 'string'
					&& conditions['and'][0].trim() !== ''
				) {
					let key = conditions['and'][0],
						hand = '=',
						value;
					and.push(mysql.format('?? = ?', conditions['and']));
				}
				if (conditions['and'].constructor === Object && Object.keys(conditions['and']).length) {

				}
				delete conditions['and'];
			}
		}
	});
}

function getOne (conditions) {
	return new Promise((resolve, reject) => {
		let project_data,
			sql = appendConditions('SELECT * FROM `projects`', conditions);

		pool.query(sql, function (err, rows) {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}

			if (!rows.length) {
				resolve(null);
				return;
			}

			project_data = {
				id: rows[0].id,
				avatar: rows[0].avatar,
				user_id: rows[0].user_id,
				is_public: rows[0].is_public,
				is_approved: rows[0].is_approved,
				created_at: rows[0].created_at,
				updated_at: rows[0].updated_at,
				attributes: {},
				members: [],
				tags: [],
				types: [],
				industry_ids: []
			};

			const sql_attributes = mysql.format('SELECT * FROM `project_data` ' +
				'WHERE project_id = ?', [project_data.id]),

				sql_tags = mysql.format('SELECT t.id, t.label FROM `project_tag` pt ' +
					'INNER JOIN `tags` t ON pt.tag_id = t.id ' +
					'WHERE project_id = ?', [project_data.id]),

				sql_industries = mysql.format('SELECT piy.industry_id id, \n' +
					'iy1.label label1, iy2.label label2, iy3.label label3\n' +
					'FROM `project_industry` piy\n' +
					'JOIN `industries` iy3 ON iy3.id = piy.industry_id\n' +
					'JOIN `industries` iy2 ON iy2.id = iy3.parent_id\n' +
					'JOIN `industries` iy1 ON iy1.id = iy2.parent_id\n' +
					'WHERE piy.project_id = ?', [project_data.id]);

				// sql_industries = mysql.format('SELECT id, parent_id, label FROM `industries` i ' +
				// 	'WHERE id in (SELECT pi.industry_id FROM `project_industry` pi WHERE pi.project_id = ?) ' +
				// 	'UNION SELECT id, parent_id, label FROM `industries` i WHERE id in (' +
				// 		'SELECT parent_id FROM `industries` i WHERE id in (' +
				// 			'SELECT pi.industry_id FROM `project_industry` pi WHERE pi.project_id = ?' +
				// 		')' +
				// 	')' +
				// 	'UNION SELECT id, parent_id, label FROM `industries` i WHERE id in (' +
				// 		'SELECT parent_id FROM `industries` i WHERE id in(' +
				// 			'SELECT parent_id FROM `industries` i WHERE id in (' +
				// 				'SELECT pi.industry_id FROM `project_industry` pi WHERE pi.project_id = ?' +
				// 			')' +
				// 		')' +
				// 	')', [project_data.id, project_data.id, project_data.id]),

				// sql_members = mysql.format('SELECT * FROM `members` ' +
				// 	'WHERE project_id = ?', [project_data.id]);

				sql_members = mysql.format('SELECT * FROM `users` ' +
					'WHERE id = ?', [project_data.user_id]),

				sql_types = mysql.format('SELECT t.* FROM `project_type` pt ' +
					'JOIN `types` t ON t.id = pt.type_id ' +
					'WHERE project_id = ?', [project_data.id]),

				sql_industry_id = mysql.format('SELECT * FROM `project_industry` ' +
					'WHERE project_id = ?', [project_data.id]);

			Promise.all([
					new Promise(function (resolve, reject) {
						pool.query(sql_attributes, function (err, rows) {
							if (err) {
								reject(err.message);
							} else {
								resolve(rows);
							}
						});
					}),
					new Promise(function (resolve, reject) {
						pool.query(sql_tags, function (err, rows) {
							if (err) {
								reject(err.message);
							} else {
								resolve(rows);
							}
						});
					}),
					new Promise(function (resolve, reject) {
						pool.query(sql_industries, function (err, rows) {
							if (err) {
								reject(err.message);
							} else {
								resolve(rows);
							}
						});
					}),
					new Promise(function (resolve, reject) {
						pool.query(sql_members, function (err, rows) {
							if (err) {
								reject(err.message);
							} else {
								resolve(rows);
							}
						});
					}),
					new Promise(function (resolve, reject) {
						pool.query(sql_types, function (err, rows) {
							if (err) {
								reject(err.message);
							} else {
								resolve(rows);
							}
						});
					}),
					new Promise(function (resolve, reject) {
						pool.query(sql_industry_id, function (err, rows) {
							if (err) {
								reject(err.message);
							} else {
								resolve(rows);
							}
						});
					})
				])
				.then(function (values) {
					// Attributes
					if (values[0].length) {
						values[0].forEach(row => {
							project_data.attributes[row.category_name] = row.category_value;
						});
					}

					// Tags
					if (values[1].length) {
						values[1].forEach(row => {
							project_data.tags.push({
								id: row.id,
								label: row.label
							});
						});
					}

					// Industries
					// let tmp = {}, ind3 = [];
					// if (values[2].length) {
					// 	const inds = values[2];
					// 	for (let i = 0; i < inds.length; i ++) {
					// 		// Root level
					// 		if (!inds[i].parent_id) {
					// 			let t1 = {
					// 				label: inds[i].label,
					// 				children: []
					// 			};
					// 			// Second level
					// 			for (let m = 0; m < inds.length; m ++) {
					// 				if (inds[m].parent_id === inds[i].id) {
					// 					let t2 = {
					// 						label: inds[m].label,
					// 						children: []
					// 					};
					// 					// Last level
					// 					for (let n = 0; n < inds.length; n ++) {
					// 						if (inds[n].parent_id === inds[m].id) {
					// 							t2.children.push({ label: inds[n].label });
					// 							ind3.push(inds[n].label);
					// 						}
					// 					}
					// 					t1.children.push(t2);
					// 				}
					// 			}
					// 			tmp[inds[i].id] = t1;
					// 		}
					// 	}
					// }
					project_data.industries_tree = values[2];
					// project_data.industries_flat = ind3;

					// Members
					if (values[3].length) {
						values[3].forEach(row => {
							project_data.members.push({
								id: row.id,
								name: row.name,
								avatar: row.avatar,
								company: row.company,
								introduction: row.introduction,
								position: row.position
							});
						});
					}

					// Types
					if (values[4].length) {
						values[4].forEach(row => {
							project_data.types.push({
								id: row.id,
								name: row.name,
								label: row.label
							});
						});
					}

					// Industry
					if (values[5].length) {
						values[5].forEach(row => {
							if (project_data.industry_ids.indexOf(row.industry_id) === -1) {
								project_data.industry_ids.push(row.industry_id);
							}
						});
					}
					resolve(project_data);
				})
				.catch(function (err) {
					reject(err);
				});
		});
	});
}

function getAttributes (project_id) {
	return new Promise(function (resolve, reject) {
		pool.query('SELECT * FROM `project_data` WHERE project_id = ?', [project_id], function (err, rows) {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			let result = {};
			// rows.forEach(function (row) {
			// 	result[row.category_name] = row.category_value;
			// });
			resolve(rows);
		});
	});
}

function getCategoryItems () {
	return new Promise(function (resolve, reject) {
		pool.query('SELECT name FROM `category_items`', function (err, rows) {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			let result = rows.map(row => row.name);
			resolve(result);
		});
	});
}

function getIndustries () {
	return new Promise((resolve, reject) => {
		pool.query('SELECT id, parent_id, label FROM `industries` ORDER BY parent_id ASC, id ASC', (err, rows) => {
			if (err) {
				reject(err);
				return;
			}

			if (!rows.length) {
				resolve(null);
				return;
			}
			let data = flatToTree(rows);
			resolve(data);
		});
	});
}

function isAddedInFavourite (id, user_id) {
	return new Promise((resolve, reject) => {
		pool.query('SELECT * FROM `favourites` WHERE project_id = ? AND user_id = ?', [id, user_id], (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			resolve(rows.length > 0);
		});
	});
}

function updateTypes (id, type_ids) {
	return new Promise((resolve, reject) => {
		if (typeof type_ids !== 'object' || type_ids.constructor !== Array || !type_ids.length) {
			resolve();
			return;
		}
		pool.query('SELECT type_id FROM `project_type` WHERE project_id = ?', [id], (err, rows) => {
			if (err) {
				console.log(err)
				reject(new httpError(500, err));
				return;
			}

			let existed = rows.map(e => parseInt(e.type_id)),
				data = [];
			type_ids.forEach(type_id => {
				if (existed.indexOf(type_id) === -1) data.push([id, type_id]);
			});
			if (!data.length) {
				resolve();
				return;
			}

			pool.query('INSERT INTO `project_type` (project_id, type_id) VALUES ?', [data], (err, rows) => {
				if (err) {
					console.log(err)
					reject(new httpError(500, err));
					return;
				}
				resolve();
			});
		});
	});
}

function update (id, data) {
	return new Promise(function (resolve, reject) {
		updateTypes(id, data.types)
			.then(() => {
				getCategoryItems()
					.then(function (category_items) {
						getAttributes(id)
							.then(function (attributes) {
								let to_insert = [],
									to_update = [];

								for (let name in data) {
									if (category_items.indexOf(name) > -1) {
										let attrs = attributes.filter(attr => attr.category_name === name);
										if (!attrs.length) {
											to_insert.push([id, name, data[name]]);
										} else {
											to_update.push({ pdid: attrs[0].id, value: data[name] });
										}
									}
								}

								let count = 2;
								if (to_insert.length) {
									pool.query('INSERT INTO `project_data` (project_id, category_name, category_value) VALUES ?', [to_insert], function (err, rows) {
										if (err) {
											reject(new httpError(500, err));
											return;
										}
										count --;
										if (!count) resolve();
									});
								} else {
									count --;
								}

								if (to_update.length) {
									bulkUpdateProjectData(id, to_update, 0, err => {
										count --;
										if (err) {
											reject(err);
											return;
										}
										if (!count) resolve();
									});
								} else {
									count --;
								}

								if (!count) resolve();
							})
							.catch(function (err) {
								reject(err);
							});
					})
					.catch(function (err) {
						reject(err);
					});
			})
			.catch(err => {
				reject(err);
			});
	});
}

function updateAvatar (id, url) {
	return new Promise((resolve, reject) => {
		pool.query('UPDATE `projects` SET avatar = ? WHERE id = ?', [url, id], function (err, rows) {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			resolve();
		});
	});
}

function bulkUpdateProjectData (id, data, i = 0, callback) {
	if (i > data.length - 1) {
		if (typeof callback === 'function') callback();
		return;
	}

	let sql = mysql.format('UPDATE `project_data` SET `category_value` = ? WHERE project_id = ? AND `id` = ?', [data[i].value, id, data[i].pdid]);
	pool.query(sql, function (err, rows) {
		if (err) {
			if (typeof callback === 'function') callback(new httpError(500, err));
			return;
		}

		i ++;
		bulkUpdateProjectData(id, data, i, callback);
	});
}