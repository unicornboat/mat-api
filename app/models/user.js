const {mysql, pool} = require('../../config/db');
const config = require('../../config/default');
const crypto = require('crypto');
const app = {};
const httpError = require('../../lib/http-error');
const jwt = require('jsonwebtoken');
const language = require('../../lib/language');
const mailer = require('../../lib/mailer');
const project  = require('./project');
const validator = require('validator');

app.addFavourite = function (user_id, project_id) {
	return new Promise((resolve, reject) => {
		pool.query('SELECT id FROM `projects` WHERE id = ?', [project_id], (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			if (!rows.length) {
				reject(new httpError(404, language('Cannot find OLD_project by ID')));
				return;
			}
			let sql = mysql.format('INSERT INTO `favourites` (user_id, project_id) ' +
				'VALUES(?, ?) ' +
				'ON DUPLICATE KEY ' +
				'UPDATE user_id = ?, project_id = ?',
				[user_id, project_id, user_id, project_id]);
			pool.query(sql, err => {
				if (err) {
					reject(new httpError(500, err));
					return;
				}
				resolve();
			});
		});
	});
};

app.changePassword = function (id, password, new_password) {
	return new Promise(function (resolve, reject) {
		let sql = mysql.format('SELECT password FROM `users` WHERE id = ?', [id]);
		pool.query(sql, function (err, rows) {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			if (!rows.length) {
				reject(new httpError(404, language('Cannot find user')));
				return;
			}

			let password_hash = crypto.createHash('sha256').update(password).digest('hex'),
				new_password_hash = crypto.createHash('sha256').update(new_password).digest('hex');
			if (rows[0].password !== password_hash) {
				reject(new httpError(401, language('Current password incorrect')));
				return;
			}

			pool.query('UPDATE `users` SET password = ? WHERE id = ?', [new_password_hash, id], function (err, rows) {
				if (err) {
					reject(new httpError(500, err));
					return;
				}
				resolve();
			});
		});
	});
};

app.clearToken = function (id) {
	return new Promise((resolve, reject) => {
		pool.query('DELETE FROM ?? WHERE ?? = ?', ['jwt_tokens', 'user_id', id], err => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			resolve();
		});
	});
};

app.create = function (company, email, name, password, phone, url) {
	let email_token = crypto.randomBytes(64).toString('hex'),
		hash = crypto.createHash('sha256').update(password).digest('hex'),
		sql = mysql.format('SELECT * FROM ?? WHERE ?? = ?', ['users', 'email', email]);
	return new Promise((resolve, reject) => {
		// Check if email is registered
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			if (rows.length) {
				reject(new httpError(409, language('This email is registered. Please login or try another email.')));
				return;
			}

			// Create a new account
			pool.query('INSERT INTO ?? SET ?', ['users', {
					company: company,
					email: email,
					email_token: email_token,
					name: name,
					password: hash,
					phone: phone,
					url: url,
				}], (err, rows) => {
				if (err) {
					reject(new httpError(500, err));
					return;
				}
				let email_body = `<p>MATCHUKUN株式会社
					このたびは、「オープンイノベーションプラットフォーム MATCHUKUN」にご登録頂きましてありがとうございました。
					アドレス確認のため下記リンクのクリックをお願いいたします。</p>
					<br>
					<p>
						<a href="${config.urlPrefix}register/confirm/${email_token}">
							${config.urlPrefix}register/confirm/${email_token}
						</a>
					</p>
					<br>
					<p>アドレス確認完了後、企業審査に入らせて頂きます。</p>
					<p>1~3営業日にて企業審査をさせて頂き、結果に関してメールにて通知させて頂きますので少々お待ちくださいませ。</p>
					<br>
					<p>その他、ご不明点がございましたらお気軽に下記までお問い合わせください。</p>
					<p>※本メールは送信専用となっております。ご返信いただいても事務局には届きませんのでご注意ください。</p>
					<p>=================================================</p>
					<p>MATCHUKUN運営事務局 noreply@rioko.studio</p>
					<p>〒123-4~ 青山ビル5F</p>
					<p>=================================================</p>`;

				sendEmail(
					'MATCHKUN運営事務局 <noreply@matchkun.rioko.studio>',
					email,
					'ご登録ありがとうございました',
					email_body
				)
					.then(() => {
						pool.query('SELECT * FROM ?? WHERE ?? = ?', ['users', 'id', rows.insertId],  (err, rows) => {
							if (err) {
								reject(new httpError(500, err));
								return;
							}

							project.create(rows[0].id, company, phone, url)
								.then(data => {
									resolve({
										id: rows[0].id,
										avatar: rows[0].avatar,
										company: rows[0].company,
										name: rows[0].name,
										phone: rows[0].phone,
										is_email_verified: rows[0].is_email_verified,
										is_phone_verified: rows[0].is_phone_verified,
										status: rows[0].status,
										url: rows[0].url,
										project: data
									});
								})
								.catch(err => {
									if (err) {
										reject(new httpError(500, err));
									}
								});
						});
					})
					.catch(err => {
						reject(new httpError(500, err));
					});
			});
		});
	});
};

app.getByEmail = function (email) {
	let sql = mysql.format('SELECT * FROM ?? WHERE ?? = ?', ['users', 'email', email]);
	return new Promise((resolve, reject) => {
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			if (!rows.length) {
				reject(new httpError(404, language('This email is not registered yet.')));
				return;
			}

			resolve({
				id: rows[0].id,
				avatar: rows[0].avatar,
				company: rows[0].company,
				name: rows[0].name,
				phone: rows[0].phone,
				is_email_verified: rows[0].is_email_verified,
				is_phone_verified: rows[0].is_phone_verified,
				status: rows[0].status,
				url: rows[0].url,
				password: rows[0].password
			});
		});
	});
};

app.get = getBy;

app.getFavourites = function (id, page = 1) {
	const items_per_page = 10;
	return new Promise((resolve, reject) => {
		let sql = mysql.format('SELECT * FROM `favourites` ' +
			'WHERE user_id = ? ' +
			'ORDER BY project_id ' +
			'LIMIT ?, ?', [id, (page - 1) * items_per_page, items_per_page]);
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			resolve(rows.map(row => row.project_id));
		});
	});
};

app.getToken = function (id, token) {
	let sql = mysql.format('SELECT * FROM `jwt_tokens` WHERE user_id = ? AND token = ?', [id, token]);
	return new Promise(function (resolve, reject) {
		pool.query(sql, function(err, rows) {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			if (!rows.length) {
				reject(new httpError(401, language('User validation failed. Please login.')));
				return;
			}

			resolve();
		});
	});
};

app.isLoggedIn = function (id) {
	return new Promise((resolve, reject) => {
		let sql = mysql.format(
			'SELECT u.id FROM ?? u INNER JOIN ?? t ON t.user_id = u.id WHERE u.id = ?',
			['users', 'jwt_tokens', id]
		);
		pool.query(sql, (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			resolve(rows.length > 0);
		});
	});
};

app.removeFavourite = function (user_id, project_id) {
	return new Promise((resolve, reject) => {
		let sql = mysql.format('DELETE FROM `favourites` WHERE user_id = ? AND project_id = ?',
			[user_id, project_id]);
		pool.query(sql, err => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			resolve();
		});
	});
};

app.resetPassword = function (email) {
	return new Promise( (resolve, reject) => {
		pool.query('SELECT * FROM `users` WHERE email = ?', [email], (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			if (!rows.length) {
				reject(new httpError(404, language('Cannot find user')));
				return;
			}

			let name = rows[0].name,
				url = `https://auba.eiicon.net/register/reset/`;
			this.resetPasswordToken(rows[0].id)
				.then(token => {
					let email_body = `<p>${name}様</p>
					<br>
					<p>ご利用いただき、誠にありがとうございます。</p>
					<br>
					<p>ご入力いただきましたメールアドレスに、パスワード再設定用のURLを送信いたしました。</p>
					<p>下記リンクをクリックしパスワードの設定を行ってください。</p>
					<br>
					<p>こちらのURLよりパスワードを設定の上ログインお願いいたします。</p>
					<p>ご登録いただき誠にありがとうございます。</p>
					<p>アドレス確認のため下記リンクのクリックをお願いいたします。</p>
					<br>
					<p>
						<a href="${config.urlPrefix}password/reset/${token}">
							${config.urlPrefix}password/reset/${token}
						</a>
					</p>
					<p>その他、ご不明点がございましたらお気軽に下記までお問い合わせください。</p>
					<p>※本メールは送信専用となっております。ご返信いただいても事務局には届きませんのでご注意ください。</p>
					<p>=================================================</p>
					<p>MATCHUKUN運営事務局 noreply@rioko.studio</p>
					<p>〒123-4~ 青山ビル5F</p>
					<p>=================================================</p>`;

					mailer.sendMail({
						from: 'MATCHKUN運営事務局 <noreply@matchkun.rioko.studio>',
						to: email,
						subject: '【MATCHKUN】ご利用ありがとうございます',
						// text: email_body,
						html: email_body
					}, (err, info) => {
						resolve();
					});
				})
				.catch(err => reject(err));
		});
	});
};

app.resetPasswordByToken = function (token, password) {
	return new Promise((resolve, reject) => {
		pool.query('SELECT * FROM `users` WHERE password_token = ?', [token], (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}

			if (!rows.length) {
				reject(new httpError(400, language('Invalid password token')));
				return;
			}

			let hash = crypto.createHash('sha256').update(password).digest('hex');
			pool.query('UPDATE `users` SET password = ? WHERE id = ?', [hash, rows[0].id], (err) => {
				if (err) {
					reject(new httpError(500, err));
					return;
				}

				this.resetPasswordToken(rows[0].id, true)
					.then(() => {
						resolve();
					})
					.catch(err => reject(err));
			});
		});
	});
};

app.resetPasswordToken = function (id, clear_token = false) {
	let token = clear_token ? '' : crypto.randomBytes(64).toString('hex');
	return new Promise((resolve, reject) => {
		pool.query('UPDATE `users` SET password_token = ? WHERE id = ?', [token, id], function (err, rows) {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			resolve(token);
		});
	});
};

app.setProfileStatus = function (user_id, public) {
	let sql = mysql.format('SELECT id FROM ?? WHERE id = ?', ['users', user_id]);
	return new Promise((resolve, reject) => {
		pool.query(sql, (err, rows) => {
			
		});
	});
};

app.update = function (id, data) {
	return new Promise((resolve, reject) => {
		updateUserData(id, data)
			.then(() => {
				resolve();
			})
			.catch(function (err) {
				reject(err);
			});
	});
};

app.updateJwtToken = function (user_id, longterm = false) {
	let sql,
		payload = { id: user_id },
		token = jwt.sign(payload, config.jwtSecret);
	return new Promise(function (resolve, reject) {
		pool.query('SELECT * FROM `jwt_tokens` WHERE user_id = ? LIMIT 1', [user_id], function (err, rows) {
				if (err) {
					reject(new httpError(500, err));
					return;
				}

				sql = !rows.length ?
					mysql.format(
						'INSERT INTO `jwt_tokens` SET ?',
						[{user_id: user_id, token: token, life: longterm ? 2592000 : 1800}]
					) :
					mysql.format(
						'UPDATE `jwt_tokens` SET token = ?, life = ? WHERE user_id = ?',
						[token, longterm ? 2592000 : 1800, user_id]
					);
				pool.query(sql, function (err) {
					if (err) {
						reject(new httpError(500, err.message));
						return;
					}
					resolve(token);
				});
			}
		);
	});
};

app.verifyEmailToken = function (token) {
	return new Promise(function (resolve, reject) {
		pool.query('SELECT * FROM `users` WHERE email_token = ?', [token], function (err, rows) {
			if (err) {
				// console.log(err);
				reject(new httpError(500, err.message));
				return;
			}
			if (!rows.length) {
				reject(new httpError(500, language('Invalid email token')));
				return;
			}

			pool.query('UPDATE `users` SET email_token = "", is_email_verified = 1 WHERE id = ?', [rows[0].id], function (err, rows) {
				if (err) {
					// console.log(err);
					reject(new httpError(500, err.message));
					return;
				}
				resolve();
			});
		});
	});
};

app.sendEmail = sendEmail;

module.exports = app;

function addTicket (user_id, qty, type, day) {
	return new Promise(function (resolve, reject) {
		let sql = mysql.format('INSERT INTO `tickets` ' +
			'(user_id, qty, expiry) ' +
			'VALUES (?, ?, DATE(DATE_ADD(NOW(), INTERVAL ? DAY)))', [user_id, qty, ]);
		pool.query(sql, function (err, rows) {
			if (err) {
				console.log(err);
				reject(new httpError(500, err.message));
				return;
			}
			resolve(rows.length ? rows[0] : null);
		});
	});
}

function getTicket (user_id, type) {
	return new Promise(function (resolve, reject) {
		pool.query('SELECT * FROM `tickets` WHERE user_id = ? AND type = ?', [user_id, type], function (err, rows) {
			if (err) {
				console.log(err);
				reject(new httpError(500, err.message));
				return;
			}
			resolve(rows.length ? rows[0] : null);
		});
	});
}

function getBy (conditions, get_password = false) {
	return new Promise(async function (resolve, reject) {
		let user_data,
			password,
			sql = 'SELECT * FROM `users`',
			keys = [],
			values = [];
		if (conditions) {
			sql += ' WHERE ';
			for (let key in conditions) {
				keys.push(key + ' = ?');
				values.push(conditions[key]);
			}
			sql += keys.join(' AND ');
			sql = mysql.format(sql, values);
		}
		pool.query(sql, function (err, rows) {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}

			if (!rows.length) {
				resolve(null);
				return;
			}

			user_data = {
				id: rows[0].id,
				avatar: rows[0].avatar,
				email: rows[0].email,
				name: rows[0].name,
				company: rows[0].company,
				duty: rows[0].duty,
				introduction: rows[0].introduction,
				position: rows[0].position,
				phone: rows[0].phone,
				is_email_verified: rows[0].is_email_verified,
				is_phone_verified: rows[0].is_phone_verified,
				is_public: rows[0].is_public,
				status: rows[0].status,
				tickets: rows[0].tickets,
				url: rows[0].url,
				favourites: []
			};

			if (get_password) user_data.password = rows[0].password;

			// Get favourite projects
			pool.query('SELECT * FROM `favourites` WHERE user_id = ?', user_data.id, function (err, rows) {
				if (err) {
					reject(new httpError(500, err));
					return;
				}

				if (rows.length) {
					user_data.favourites = rows.map(e => e.project_id);
				}

				resolve(user_data);
			});
		});
	});
}

function updateUserData (id, data) {
	return new Promise((resolve, reject) => {
		let payload = {};
		pool.query('SELECT avatar, name, company, position, introduction, duty, is_public FROM `users` WHERE id = ? LIMIT 1', [id], (err, rows) => {
			if (err) {
				reject(new httpError(500, err));
				return;
			}
			if (!rows.length) {
				reject(new httpError(404, language('Cannot find user')));
				return;
			}
			for (let key in data) {
				let value = data[key];
				switch (key) {
					case 'avatar':
						// if (!validator.isURL(value.trim())) {
						// 	reject(new httpError(500, language('Invalid URL')));
						// 	return;
						// }
						payload[key] = value.trim();
						break;
					case 'email':
					case 'email_token':
					case 'position':
						payload[key] = value.trim();
						break;
					case 'is_email_verified':
						payload[key] = value;
						break;
					case 'name':
						if (!validator.isLength(value.trim(), {max: 10, min: 1})) {
							reject(new httpError(400, language('Incorrect name length')));
							return;
						}
						payload[key] = value.trim();
						break;
					case 'company':
						if (!validator.isLength(value.trim(), {max: 50, min: 1})) {
							reject(new httpError(400, language('Incorrect company length')));
							return;
						}
						payload[key] = value.trim();
						break;
					case 'introduction':
						if (!validator.isLength(value.trim(), {max: 500, min: 1})) {
							reject(new httpError(400, language('Incorrect introduction length')));
							return;
						}
						payload[key] = value.trim();
						break;
					case 'duty':
						if (!validator.isLength(value.trim(), {max: 1000, min: 1})) {
							reject(new httpError(400, language('Incorrect mission length')));
							return;
						}
						payload[key] = value.trim();
						break;
					case 'is_public':
						if (parseInt(value) !== 1 && parseInt(value) !== 0) {
							reject(new httpError(400, language('Incorrect profile publicity setting')));
							return;
						}
						payload[key] = value;
						break;
				}
			}
			pool.query('UPDATE `users` SET ? WHERE id = ?', [payload, id], (err, rows) => {
				if (err) {
					reject(new httpError(500, err));
					return;
				}
				resolve();
			});
		});
	});
}

function sendEmail (sender, recipient, subject, html, text) {
	return new Promise((resolve, reject) => {
		let data = {
			from: sender,
			to: recipient,
			subject: subject
		};

		if (html) data.html = html;
		if (text) data.text = text;
		mailer.sendMail(data, (err, info) => {
			if (err) {
				console.log(err)
				reject(new httpError(500, err));
				return;
			}
			resolve();
		});
	});
}