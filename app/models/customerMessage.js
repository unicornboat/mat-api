const {mysql, pool} = require('../../config/db');
const app = {};
const httpError = require('../../lib/http-error');
const language = require('../../lib/language');
const moment = require('moment');
const validator = require('validator');

app.add = function (name, company_name, email, content) {
	return new Promise((resolve, reject) => {
		let errors = [];
		if (!validator.isLength(name, {max: 10, min: 1})) errors.push(language('Incorrect name length'));
		if (!validator.isLength(company_name, {max: 20, min: 1})) errors.push(language('Company name is required'));
		if (!validator.isEmail(email)) errors.push(language('Incorrect email format'));
		if (!validator.isLength(content, {max: 500, min: 20})) errors.push(language('Content cannot be shorter than %s or longer than %s', 20, 500));
		if (errors.length) {
			reject(new httpError(400, errors));
			return;
		}

		let sql = mysql.format('INSERT INTO `customer_messages` SET ?', {
			name: name,
			company_name: company_name,
			email: email,
			content: content,
			created_at: moment().format('YYYY-MM-DD HH:mm:ss')
		});
		pool.query(sql, err => {
			if (err) {
				reject(new httpError(500, err.message));
				return;
			}
			resolve();
		});
	});
};

module.exports = app;