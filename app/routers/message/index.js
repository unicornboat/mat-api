const auth = require('../../../lib/jwter');
const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use('/list', auth, require('./list'));
router.use('/add', auth, require('./add'));
router.use('/:id', auth, require('./get'));

module.exports = router;