const express = require('express');
const messageDao = require('../../dao/message');
const router = express.Router({mergeParams: true});

router.get('/', async function (req, res) {
	try {
		res.send({data: await messageDao.getList(req.userId)});
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;