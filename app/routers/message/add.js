const express = require('express');
const messageDao = require('../../dao/message');
const router = express.Router({mergeParams: true});
const userDao = require('../../dao/user');

router.post('/', async function (req, res, next) {
	try {
		let receiver_id = req.body.receiver_id ? req.body.receiver_id : null,
			project_id = req.body.project_id ? req.body.project_id : null,
			content = req.body.content ? req.body.content.trim() : '',
			time = req.body.time ? req.body.time : null;

		if (!time) {
			time = new Date().toISOString()
				.replace(/\.\d{3}Z/g, '')
				.replace('T', ' ');
		}

		if (!receiver_id || !project_id || !content.length)
			throw new Error('メッセージの送信に失敗しました');

		await messageDao.add(req.userId, receiver_id, project_id, content, time);
		res.send();
	} catch (err) {
		console.log(__filename, err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;