const express = require('express');
const messageDao = require('../../dao/message');
const router = express.Router({mergeParams: true});

router.post('/', async function (req, res) {
	try {
		let id1 = parseInt(req.userId),
			id2 = parseInt(req.params.id);

		// if (isNaN(id1) || isNaN(id2))
		// 	throw new Error('何かがうまくいかなかった。 もう一度やり直してください[1]');

		// if (parseInt(req.userId) !== id1 && parseInt(req.userId) !== id2)
		// 	throw new Error('何かがうまくいかなかった。 もう一度やり直してください[2]');

		let data = await messageDao.getMessages(id1, id2);
		await messageDao.markAsRead(id1, id2);
		res.send({data: data});
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;