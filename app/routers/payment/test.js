const secret = process.env.STRIPE_SECRET;
const express = require('express');
// const projectDao = require('../../dao/OLD_project');
const stripe = require('stripe')(secret);
const router = express.Router({mergeParams: true});

router.post('/', async function (req, res) {
	try {
		const paymentIntent = await stripe.paymentIntents.create({
			amount: 1000,
			currency: 'aud',
			payment_method_types: ['card'],
			receipt_email: 'jenny.rosen@example.com',
		});
		res.send({data: paymentIntent});
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;