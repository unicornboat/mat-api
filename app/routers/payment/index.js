const auth = require('../../../lib/jwter');
const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use('/test', require('./test'));

module.exports = router;


