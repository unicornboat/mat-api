const auth = require('../../lib/jwter');
// const aws3 = require('../../lib/aws3');
const bodyParser = require('body-parser');
const express = require('express');
const formidable = require('formidable');
const language = require('../../lib/language');
const project  = require('../models/project');
const router = express.Router();
const userDao = require('../dao/user');
// const uuid = require('uuid-random');

// Middlewares
router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/industries', async function (req, res) {
	try {
		let data = await project.getIndustries();
		res.send({data: data});
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			messages: err.message
		});
	}
});

router.post('/industries', auth, function (req, res) {
	let a_ids = typeof req.body.ids === 'object' && req.body.ids.constructor === Array ? req.body.ids : [];
	if (a_ids.length > 10) {
		res.status(400);
		res.send({
			error: 1,
			messages: ['10件以上は登録できません']
		});
		return;
	}
	project.get({user_id: req.userId})
		.then(data => {
			a_ids = a_ids.filter(each => each !== data.id);
			project.updateIndustries(data.id, a_ids)
				.then(data => {
					console.log(data)
					res.send({error: 0, data: data});
				})
				.catch(err => {
					respondError(res, err);
				});
		})
		.catch(err => {
			respondError(res, err);
		});
});

router.get('/types', function (req, res) {
	project.getTypes()
		.then(data => {
			res.send({
				error: 0,
				data: data
			});
		})
		.catch(err => {
			respondError(res, err);
		});
});

// Get OLD_project data with user ID
router.get('/:id/:userId', auth, function (req, res) {
	project.get({ user_id: req.userId })
		.then(async function (project_data) {
			if (!project_data) {
				respondError(res, new httpError(404, language('Cannot find OLD_project')));
				return;
			}

			user.isLoggedIn(req.userId).then(is_logged_in => {
				data.is_user_favourite = is_logged_in;
				res.send({
					error: 0,
					data: data
				});
			}).catch(err => {
				respondError(res, err);
			});
		})
		.catch(err => {
			respondError(res, err);
		});
});

router.post('/avatar', auth, function (req, res) {
	user.getToken(req.userId, req.token).then(() => {
		let form = new formidable.IncomingForm();
		form.parse(req, async function (err, fields, files) {
			if (!files.file) {
				respondError(res, new httpError(400, language('No files found')));
				return;
			}

			let name = 'OLD_project-avatar-' + uuid(), file = files.file;
			switch (file.type) {
				case 'image/jpeg':
					name += '.jpg'
					break;
				case 'image/png':
					name += 'png';
					break;
				default:
					respondError(new httpError(400, language('File type is not allowed')));
					return;
			}

			project.get({ user_id: req.userId })
				.then(async function (project_data) {
					if (!project_data) {
						respondError(res, new httpError(404, language('Cannot find OLD_project')));
						return;
					}

					let url = await aws3.upload(name, file.type, file.path);
					project.updateAvatar(project_data.id, url)
						.then(function () {
							res.send({
								error: 0,
								url: url
							});
						})
						.catch(function (err) {
							respondError(res, err);
						});
				})
				.catch(function (err) {
					respondError(res, err);
				});
		});
	}).catch(function (err) {
		console.log(err);
		respondError(res, err);
	});
});

router.post('/public/:is_public', auth, function (req, res) {
	user.getToken(req.userId, req.token)
		.then(function () {
			project.get({ user_id: req.userId })
				.then(function (project_data) {
					project.setPublic(project_data.id, req.params.is_public)
						.then(function () {
							res.send({ error: 0 });
						})
						.catch(function (err) {
							console.log(err);
							respondError(res, err);
						});
				})
				.catch(err => {
						console.log(err);
					respondError(res, err);
				});
		})
		.catch(function (err) {
			console.log(err);
			respondError(res, err);
		});
});

router.route('/:id')
	.all(function (req, res, next) {
		next();
	})
	// .delete(function (req, res, next) {})
	.get(function (req, res, next) {
		project.getById(req.params.id, req.userId)
			.then(data => {
				if (!data) {
					res.status(404);
					res.send({
						error: 1,
						message: 'ビジネス情報が見つかりません'
					});
					return;
				}
				res.send({data: data});
			})
			.catch(err => {
				console.log(err)
				res.status(500);
				res.send({
					error: 1,
					message: err.message
				});
			});
	})
	.post(auth, function (req, res, next) {
		project.update(req.params.id, req.body)
			.then(function () {
				project.get()
					.then(function (project_data) {
						res.send({
							error: 0,
							data: project_data
						});
					})
					.catch(function (err) {
						respondError(res, err);
					});
			}).catch(function (err) {
				respondError(res, err);
			});
	})
	.put(function (req, res, next) {});

module.exports = router;

function respondError (res, err) {
	console.log(err);
	res.status(err.getCode());
	res.send({
		error: 1,
		messages: err.getMessage()
	});
}