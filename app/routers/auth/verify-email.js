const configDao = require('../../dao/config');
const express = require('express');
const language = require('../../../lib/language');
const mailer = require('../../../lib/mailer');
const router = express.Router({mergeParams: true});
const userDao = require('../../dao/user');

router.post('/', async function (req, res) {
	try {
		let receiver = await configDao.get('signup_notification_email_to'),
			user = await userDao.getByConditions({email_token: req.body.token ? req.body.token : ''});
		if (!user) throw new Error(language('Invalid email token'));

		await userDao.update({
			email_token: '',
			is_email_verified: 1
		}, {id: user.id});

		await mailer.send(receiver, 'signup_notification_email', {
			'%%company%%': user.project.attributes.company_name,
			'%%name%%': user.name
		});

		res.send();
	} catch (err) {
		console.log(__filename, err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;