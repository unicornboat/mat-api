const crypto = require('crypto');
const express = require('express');
const language = require('../../../lib/language');
const router = express.Router({mergeParams: true});
const userDao = require('../../dao/user');
const validator = require('validator');

router.post('/', async function (req, res) {
	try {
		if (!req.body.token || !req.body.token.trim().length)
			throw new Error(language('Invalid password token'));
		if (!req.body.password1 || !validator.isLength(req.body.password1, {min: 8}))
			throw new Error(language('Password is too short'));
		if (!req.body.password2 || !validator.isLength(req.body.password2, {min: 8}))
			throw new Error(language('Password is too short'));
		if (req.body.password1 !== req.body.password2)
			throw new Error(language('The two passwords entered do not match'));

		let user = await userDao.getByConditions({password_token: req.body.token});
		if (!user) throw new Error(language('Invalid password token'));
		await userDao.update({
			password_token: '',
			password: crypto.createHash('sha256').update(req.body.password1).digest('hex')
		}, {id: user.id});
		res.send();
	} catch (err) {
		console.log(err)
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;