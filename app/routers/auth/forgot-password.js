const express = require('express');
const language = require('../../../lib/language');
const mailer = require('../../../lib/mailer');
const router = express.Router({mergeParams: true});
const userDao = require('../../dao/user');
const validator = require('validator');

router.post('/', async function (req, res) {
	try {
		if (!validator.isEmail(req.body.email)) throw new Error(language('Incorrect email format'));
		let user = await userDao.getByConditions({email: req.body.email});
		if (!user) throw new Error(language('Cannot find user'));
		let password_token = await userDao.generatePasswordToken(req.body.email);

		await mailer.send(user.email, 'reset_password_email', {
			'%%name%%': user.name,
			'%%password_token%%': password_token
		});
		res.send();
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;