const customerMessageDao = require('../../dao/customer_message');
const express = require('express');
const language = require('../../../lib/language');
const router = express.Router({mergeParams: true});
const validator = require('validator');

router.post('/', async function (req, res) {
	try {
		if (!req.body.email || !validator.isEmail(req.body.email)) throw new Error(language('Incorrect email format'));
		if (!req.body.name || !req.body.name.trim().length) throw new Error(language('Your name is required'));
		if (!req.body.company || !req.body.company.trim().length) throw new Error(language('Company name is required'));
		if (!req.body.content || !validator.isLength(req.body.content.trim(), {max: 500, min: 20}))
			throw new Error(language('Content cannot be shorter than %s or longer than %s', 20, 500));

		await customerMessageDao.add(
			req.body.name.trim(),
			req.body.company.trim(),
			req.body.email,
			req.body.content.trim()
		);
		res.send();
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;