const crypto = require('crypto');
const express = require('express');
const language = require('../../../lib/language');
const router = express.Router({mergeParams: true});
const tokenDao = require('../../dao/token');
const userDao = require('../../dao/user');

router.post('/', async function (req, res) {
	try {
		let user = await userDao.getByConditions({
			email: req.body.email ? req.body.email : '',
			password: req.body.password ? hash(req.body.password) : hash(''),
		});

		if (!user) throw new Error(language('Please check your email or password'));
		if (!user.is_email_verified) throw new Error(language('Your email address is not verified yet. <a href="/#/resend-confirmation">Resend confirmation email</a>'));
		if (!user.is_approved) throw new Error(language('Your account is not activated yet. We will notify you once it\'s approved.'));
		let jwt_token = await tokenDao.update(user.id, req.body.longterm);
		res.send({
			data: user,
			token: jwt_token
		});
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;

function hash (str) {
	return crypto.createHash('sha256').update(str).digest('hex');
}