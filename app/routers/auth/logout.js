const express = require('express');
const router = express.Router({mergeParams: true});
const tokenDao = require('../../dao/token');

router.post('/', async function (req, res) {
	try {
		await tokenDao.remove(req.userId);
		res.send();
	} catch (err) {
		console.log(err)
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;