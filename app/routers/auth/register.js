const express = require('express');
const language = require('../../../lib/language');
const mailer = require('../../../lib/mailer');
const router = express.Router({mergeParams: true});
const tokenDao = require('../../dao/token');
const userDao = require('../../dao/user');
const validator = require('validator');

router.post('/', async function (req, res) {
	try {
		let company = req.body.company ? req.body.company.trim() : '',
			email = req.body.email ? req.body.email.trim() : '',
			name = req.body.name ? req.body.name.trim() : '',
			password1 = req.body.password1 ? req.body.password1 : '',
			password2 = req.body.password2 ? req.body.password2 : '',
			phone = req.body.phone ? req.body.phone.trim() : '',
			url = req.body.url ? req.body.url.trim() : '';

		if (!validator.isLength(company, {min: 1})) throw new Error(language('Company name is required'));
		if (!validator.isEmail(email)) throw new Error(language('Incorrect email format'));
		if (!validator.isLength(name, {min: 1})) throw new Error(language('Your name is required'));
		if (!validator.isLength(password1, {min: 8})) throw new Error(language('Password is too short'));
		if (password1 !== password2) throw new Error(language('The two passwords entered do not match'));
		if (!validator.isLength(phone, {max: 11, min: 10})) throw new Error('Invalid phone number');
		if (!validator.isNumeric(phone, {no_symbols: true})) throw new Error(language('Do not use symbols in phone number'));
		if (!validator.isURL(url)) throw new Error(language('Invalid URL'));
		if (await userDao.isEmailExist(email)) throw new Error(language('Cannot use this email'));


		let user = await userDao.create(email, name, password1, company, phone, url);
		let jwt_token = await tokenDao.update(user.id);
		await mailer.send(email, 'signup_confirmation_email', {
			'%%company%%': company,
			'%%name%%': name,
			'%%email_token%%': user.email_token
		});

		res.send({
			data: user,
			token: jwt_token,
		});
	} catch (err) {
		console.log(err)
		res.send({error: 1, message: err.message});
	}
});

module.exports = router;