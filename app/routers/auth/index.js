const bodyParser = require('body-parser');
const auth = require('../../../lib/jwter');
const express = require('express');
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use('/message-customer-service', require('./message-customer-service'));
router.use('/forgot-password', require('./forgot-password'));
router.use('/login', require('./login'));
router.use('/logout', auth, require('./logout'));
router.use('/register', require('./register'));
router.use('/resend', auth, require('./resend'));
router.use('/reset-password-by-token', require('./reset-password-by-token'));
router.use('/verify-email', require('./verify-email'));

module.exports = router;
