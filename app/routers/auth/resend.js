const configDao = require('../../dao/config');
const express = require('express');
const mailer = require('../../../lib/mailer');
const router = express.Router({mergeParams: true});
const userDao = require('../../dao/user');

router.post('/', async function (req, res) {
	try {
		let user = await userDao.get(req.userId, true);

		await mailer.send(user.email, 'signup_confirmation_email', {
			'%%company%%': user.company,
			'%%name%%': user.name,
			'%%email_token%%': user.email_token
		});
		res.send();
	} catch (err) {
		console.log(err)
		res.send({error: 1, message: err.message});
	}
});

module.exports = router;