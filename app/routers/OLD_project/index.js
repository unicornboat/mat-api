const auth = require('../../../lib/jwter');
const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use('/get_industries', auth, require('./get-industries'));
router.use('/delete_industry/:id', auth, require('./delete-industry'));
router.use('/get_types', auth, require('./get-types'));

module.exports = router;
