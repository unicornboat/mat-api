const express = require('express');
const router = express.Router({mergeParams: true});
const projectDao = require('../../dao/project');

router.delete('/', async function (req, res) {
	try {
		let project_id = req.params.id;
		await projectDao.deleteIndustry(req.userId, req.params.id);
		res.send();
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;