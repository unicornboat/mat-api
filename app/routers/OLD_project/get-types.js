const express = require('express');
const router = express.Router({mergeParams: true});
const projectDao = require('../../dao/project');

router.get('/', async function (req, res) {
	try {
		let data = await projectDao.getIndustryTypes();
		res.send(data);
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			messages: err.message
		});
	}
});

module.exports = router;