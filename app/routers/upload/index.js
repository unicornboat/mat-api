const auth = require('../../../lib/jwter');
const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use('/OLD_project/avatar', auth, require('./project-avatar'));
router.use('/user/avatar', auth, require('./user-avatar'));

module.exports = router;


