const crypto = require('crypto');
const express = require('express');
const fs = require('fs');
const language = require('../../../lib/language');
const multer = require('multer');
const projectDao = require('../../dao/project');
const router = express.Router({mergeParams: true});
const upload = multer({dest: 'public/uploads/'});

router.post('/', upload.single('file'), async function (req, res) {
	try {
		let project = await projectDao.getByUserId(req.userId),
			file = req.file,
			rand_hex = crypto.randomBytes(32).toString('hex'),
			filename = 'p_' + project.id + '_' + rand_hex,
			filepath = 'public/uploads/';

		switch (file.mimetype) {
			case 'image/jpeg':
				filename += '.jpg';
				break;
			case 'image/png':
				filename += '.png';
				break;
			case 'image/gif':
				filename += '.gif';
				break;
			default:
				fs.unlinkSync(file.path);
				throw new Error(language('Image type must be jpeg, png, or gif'));
		}
		filepath += filename;

		fs.renameSync(file.path, filename);
		await projectDao.update({avatar: filename}, {id: project.id});
		res.send({data: filename});
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;