const crypto = require('crypto');
const express = require('express');
const fs = require('fs');
const language = require('../../../lib/language');
const multer = require('multer');
const router = express.Router({mergeParams: true});
const upload = multer({dest: 'public/uploads/'});
const userDao = require('../../dao/user');

router.post('/', upload.single('file'), async function (req, res) {
	try {
		let user = await userDao.get(req.userId),
			file = req.file,
			rand_hex = crypto.randomBytes(32).toString('hex'),
			filename = 'u_' + user.id + '_' + rand_hex,
			filepath = 'public/uploads/';

		switch (file.mimetype) {
			case 'image/jpeg':
				filename += '.jpg';
				break;
			case 'image/png':
				filename += '.png';
				break;
			case 'image/gif':
				filename += '.gif';
				break;
			default:
				fs.unlinkSync(file.path);
				throw new Error(language('Image type must be jpeg, png, or gif'));
		}
		filepath += filename;

		fs.renameSync(file.path, filepath);
		await userDao.update({avatar: filename}, {id: user.id});
		res.send({data: filename});
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;