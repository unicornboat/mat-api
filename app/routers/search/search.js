const express = require('express');
const projectDao = require('../../dao/project');
const router = express.Router({mergeParams: true});

router.post('/', async function (req, res) {
	try {
		let items_per_page = 10,
			keywords = req.body.keywords ? req.body.keywords : '',
			order_by = typeof req.body.order_by === 'object' && req.body.order_by.constructor === Array ? req.body.order_by : [],
			search_type = typeof req.body.search_type === 'string' ? req.body.search_type.trim() : '',
			industries = typeof req.body.industries === 'object' && req.body.industries.constructor === Array ? req.body.industries : [],
			filters = typeof req.body.filters === 'object' && req.body.filters.constructor === Array ? req.body.filters : [],
			page = parseInt(req.body.page) > 0 ? parseInt(req.body.page) : 1;

		let projects = await projectDao.search(keywords, search_type, filters, industries, page, items_per_page, order_by);
		res.send({data: projects});
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;