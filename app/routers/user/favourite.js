const express = require('express');
const language = require('../../../lib/language');
const router = express.Router({mergeParams: true});
const projectDao = require('../../dao/project');
const userDao = require('../../dao/user');

router.delete('/', async function (req, res) {
	try {
		if (req.params.id) {
			let user = await userDao.get(req.userId),
				project = await projectDao.get({id: req.params.id});
			if (!project) throw new Error(language('Cannot find OLD_project'));
			await userDao.removeFromFavourites(user.id, project.id);
		}
		res.send();
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

router.post('/', async function (req, res) {
	try {
		if (req.params.id) {
			let user = await userDao.get(req.userId),
				project = await projectDao.get({id: req.params.id});
			if (!project)
				throw new Error(language('Cannot find OLD_project'));
			if (user.project.id === project.id)
				throw new Error(language('Cannot add your own company to favourites'));
			await userDao.addToFavourites(user.id, project.id);
		}
		res.send();
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;