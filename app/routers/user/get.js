const express = require('express');
const language = require('../../../lib/language');
const router = express.Router({mergeParams: true});
const userDao = require('../../dao/user');

router.get('/', async function (req, res) {
	try {
		let user = await userDao.getByConditions({id: req.userId});
		if (!user) throw new Error(language('Cannot find user'));
		res.send({data: user});
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;