const auth = require('../../../lib/jwter');
const bodyParser = require('body-parser');
const express = require('express');
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.use('/favourites/:page', auth, require('./favourites'));
router.use('/favourite/:id', auth, require('./favourite'));
router.use('/password', auth, require('./password'));
router.use('/update', auth, require('./update'));
router.get('/', auth, require('./get'));

module.exports = router;
