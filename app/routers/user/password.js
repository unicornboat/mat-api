const crypto = require('crypto');
const express = require('express');
const mailer = require('../../../lib/mailer');
const router = express.Router({mergeParams: true});
const userDao = require('../../dao/user');

router.post('/', async function (req, res) {
	try {
		if (!req.body.password) throw new Error(language('Password is required'));
		if (!req.body.new_password1) throw new Error(language('Password is required'));
		if (!req.body.new_password2) throw new Error(language('Password is required'));
		if (req.body.new_password1.length < 8) throw new Error(language('Password is too short'));
		if (req.body.new_password2.length < 8) throw new Error(language('Password is too short'));
		if (req.body.new_password1 !== req.body.new_password2)
			throw new Error(language('The two passwords entered do not match'));

		let user = await userDao.getByConditions({
			id: req.userId,
			password: crypto.createHash('sha256').update(req.body.password).digest('hex')
		});
		if (!user) throw new Error(language('Current password incorrect'));
		await userDao.update({
			password: crypto.createHash('sha256').update(req.body.new_password1).digest('hex')
		}, {id: user.id});

		await mailer.send(user.email, 'password_changed_email', {
			'%%name%%': user.name
		});

		res.send();
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;