const crypto = require('crypto');
const express = require('express');
const language = require('../../../lib/language');
const mailer = require('../../../lib/mailer');
const router = express.Router({mergeParams: true});
const projectDao = require('../../dao/project');
const userDao = require('../../dao/user');
const validator = require('validator');

router.post('/', async function (req, res) {
	try {
		if (!Object.keys(req.body).length) throw new Error(language('Invalid user data'));
		let user = await userDao.get(req.userId);
		if (!user) throw new Error(language('Cannot find user'));

		let email = req.body.email ? req.body.email : null,
			email_token = crypto.randomBytes(16).toString('hex'),
			change_email = false;

		if (email && user.email !== email) {
			if (!validator.isEmail(email)) throw new Error(language('Incorrect email format'));
			let is_email_used = await userDao.isEmailExist(email);
			if (is_email_used) throw new Error(language('Cannon use this email'));

			change_email = true;
			req.body.email_token = email_token;
			req.body.is_email_verified = 0;
		} else {
			delete req.body.email;
		}

		if (req.body.company) {
			await projectDao.updateData({company_name: req.body.company}, {user_id: req.userId});
		}

		await userDao.update(req.body, {id: user.id});
		user = await userDao.get(req.userId);

		if (change_email) {
			await mailer.send(email, 'email_changed_email', {
				'%%name%%': user.name
			});
		}

		res.send({
			data: await userDao.get(user.id),
			isEmailChanged: change_email
		});
	} catch (err) {
		console.log(err);
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;