const express = require('express');
const projectDao = require('../../dao/project');
const router = express.Router({mergeParams: true});

router.get('/', async function (req, res) {
	try {
		let projects = [],
			page = !req.params.page || parseInt(req.params.page) <= 0 ? 1 : parseInt(req.params.page),
			project_ids = await projectDao.getFavourites(req.userId, page - 1, 10);
		if (project_ids.length) {
			for (let i = 0; i < project_ids.length; i ++) {
				projects.push(await projectDao.get({id: project_ids[i]}));
			}
		}
		res.send({data: projects});
	} catch (err) {
		res.send({
			error: 1,
			message: err.message
		});
	}
});

module.exports = router;