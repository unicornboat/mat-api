const adminDao = require('../../dao/admin');
const config = require('../../../config/default');
const crypto = require('crypto');
const express = require('express');
const jwt = require('jsonwebtoken');
const router = express.Router({mergeParams: true});

router.post('/', async function (req, res, next) {
	let user = await adminDao.getByConditions({
		username: req.body.username ? req.body.username : '',
		password: req.body.password ? hash(req.body.password) : hash(''),
	});

	if (!user) {
		res.data = {
			error: 1,
			message: 'ユーザー名やパスワードを確認してください'
		};
	} else {
		let token = jwt.sign({id: user.id}, config.jwtSecret);
		await adminDao.update({token: token}, {id: user.id});
		res.data = {data: token};
	}

	next();
});

module.exports = router;

function hash (str) {
	const md5 = crypto.createHash('md5').update(str).digest('hex');
	return crypto.createHash('sha256').update(md5).digest('hex');
}