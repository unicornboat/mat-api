const adminDao = require('../../dao/admin');
const express = require('express');
const router = express.Router({mergeParams: true});

router.get('/', async function (req, res, next) {
	let user = await adminDao.getByConditions({id: req.userId});
	if (!user) {
		res.send({
			error: 1,
			message: 'ユーザーが見つかりません'
		});
		return;
	}

	res.data = {data: user};
	next();
});

module.exports = router;