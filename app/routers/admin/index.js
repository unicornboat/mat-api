const bodyParser = require('body-parser');
const auth = require('../../../middleware/admin_auth');
const express = require('express');
const router = express.Router();

router.use(bodyParser.urlencoded({ extended: true }));
router.use(bodyParser.json());

router.get('/', auth.authCheck, require('./get'), (req, res) => res.send(res.data));
router.use('/login', require('./login'), (req, res) => res.send(res.data));

module.exports = router;
