const adminDao = require('../../dao/admin');
const crypto = require('crypto');
const express = require('express');
const router = express.Router({mergeParams: true});
const tokenDao = require('../../dao/token');

router.post('/', async function (req, res, next) {
	let user = await adminDao.getByConditions({
		username: req.body.username ? req.body.username : '',
		password: req.body.password ? hash(req.body.password) : hash(''),
	});
	if (!user) {
		res.send({
			error: 1,
			message: 'ユーザー名やパスワードを確認してください'
		});
		return;
	}

	let token = tokenDao.update(user.id);
	res.data = {
		data: user,
		token: token
	};
	next();
});

module.exports = router;

function hash (str) {
	const md5 = crypto.createHash('md5').update(str).digest('hex');
	return crypto.createHash('sha256').update(md5).digest('hex');
}