const config = require('../config/default');
const jwt = require('jsonwebtoken');
const language = require('./language');

module.exports = function (options) {
	return function (req, res, next) {
		const auth_header = req.headers.authorization,
			url = req.url.replace(`/${config.apiPrefix}/v${config.apiVersion}/`, '');

		// Skip the url in the excludes list
		if (typeof options.excludes === 'object' && options.excludes.length) {
			let pass = false;
			options.excludes.forEach(exclude => {
				if (typeof exclude === 'string' && exclude === url) pass = true;
				if (typeof exclude === 'object' && exclude.constructor === RegExp) {
					if (exclude.test(url)) pass = true;
				}
			});
			if (pass) {
				next();
				return;
			}
		}

		if (auth_header) {
			const token = auth_header.split(' ')[1];
			jwt.verify(token, config.jwtSecret, (err, data) => {
				if (err || typeof data.id !== 'number') {
					res.status(403);
					res.send({
						error: 1,
						messages: [language('Invalid authorisation, please login first') + '[1]']
					});
					return;
				}
				req.token = token;
				req.userId = data.id;
				next();
			});
			return;
		}

		res.status(401);
		res.send({
			error: 1,
			messages: [language('Invalid authorisation, please login first') + '[2]']
		});
	}
}