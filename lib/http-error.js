class HttpError {
	#code = 200;
	#message;

	constructor (code, message) {
		this.#code = parseInt(code);
		if (typeof message === 'string') {
			this.#message = [message];
		} else {
			this.#message = message;
		}
	}

	getCode () {
		return this.#code;
	}

	getMessage () {
		return this.#message;
	}
}

module.exports = HttpError;