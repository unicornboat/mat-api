const app = {};
const AWS = require('aws-sdk');
const { Buffer } = require('buffer');
const config = require('../config/default');
const fs = require('fs');
const httpError = require('./http-error');

AWS.config.update({ region: config.awsS3Region });

// const s3 = new AWS.S3({ apiVersion: config.awsApiVersion });
const s3 = new AWS.S3({
	accessKeyId: config.awsAccessKeyId,
	secretAccessKey: config.awsSecretAccessKey
});

app.upload = function (name, type, path) {
	return new Promise(resolve => {
		fs.readFile(path, (err, data) => {
			if (err) throw err;
			let params = {
					Bucket:  config.awsBucketName,
					Key: name,
					Body: data,
					ACL: 'public-read',
					ContentType: type
				};
			s3.upload(params, function(err, data) {
				if (err) {
					console.log('AWS S3 Error', err.stack);
					throw new httpError(500, err.message);
				}
				resolve(data.Location);
				// console.log(`File uploaded successfully at ${data.Location}`)
			});
		});
	});
};

module.exports = app;