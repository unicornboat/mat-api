const config = require('../config/default');
const configDao = require('../app/dao/config');
const nodemailer = require('nodemailer');

/**
 * Initialise a nodemailer transporter instance
 *
 * @type {Mailer<options>}
 */
const transporter = nodemailer.createTransport({
	host: config.smtpServer,
	port: 465,
	secure: true, // true for 465, false for other ports
	auth: {
		user: config.smtpUsername,
		pass: config.smtpPassword
	},
});

/**
 * Send email by using template
 *
 * @param recipient {string}  Email address
 * @param template  {string}  Template prefix
 * @param data      {object}  Strings to be replaced
 * @returns         {Promise}
 */
const send = async function (recipient, template, data) {
	let subject = await configDao.get(template + '_subject'),
		sender = await configDao.get(template + '_from'),
		html = await configDao.get(template + '_html'),
		text = await configDao.get(template + '_text'),
		website_url = await configDao.get('website_url');

	// Replace other strings
	for (let key in data) {
		let re = new RegExp(key, 'g');
		html = html.replace(re, data[key]);
		text = text.replace(re, data[key]);
	}

	// Replace URL in the email template
	html = html.replace(/%%url%%/g, website_url);
	text = text.replace(/%%url%%/g, website_url);

	await outgo(sender, recipient, subject, html, text);
};

/**
 *
 * @param sender    {string}  Sender's email address
 * @param recipient {string}  Recipient's email address
 * @param subject   {string}  Email subject line
 * @param html      {string}  Body in HTML format
 * @param text      {string}  Body in text format
 * @returns         {Promise}
 */
const outgo = function (sender, recipient, subject, html, text) {
	return new Promise(function (resolve, reject) {
		let data = {
			from: sender,
			to: recipient,
			subject: subject
		};

		if (html) data.html = html;
		if (text) data.text = text;
		transporter.sendMail(data, function (err, info) {
			if (err) {
				reject(err);
			} else {
				resolve();
			}
		});
	});
}

module.exports = {
	send: send
};