const config = require('../../config/default');
const mac = new qiniu.auth.digest.Mac(config.qiniuAppKey, config.qiniuSecret);
const qiniuConfig = new qiniu.conf.Config();
const bucketManager = new qiniu.rs.BucketManager(mac, qiniuConfig);
const publicBucketDomain = 'http://if-pbl.qiniudn.com';

// 公开空间访问链接
const publicDownloadUrl = bucketManager.publicDownloadUrl(publicBucketDomain, key);
// console.log(publicDownloadUrl);
