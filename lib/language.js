const config = require('../config/default');
const code = config.languageCode;
const I18N = require('../data/i18n.json');

module.exports = function () {
	if (typeof I18N !== 'object') throw new Error('Cannot load i18n.json. Make sure it\'s in the /data directory.');
	if (!arguments.length) return '';
	let args = Array.prototype.filter.call(arguments, (arg, i) => i !== 0),
		dest, lang,
		src = arguments[0];

	// Cannot find a key matching the source string
	if (!I18N.hasOwnProperty(src)) return src;
	lang = I18N[src];

	// Assign the source string to the temporary package if code not available
	if (!lang.hasOwnProperty(code)) lang[code] = src;
	dest = lang[code].split('%s');
	return dest.reduce((accum, each, i) => accum += (args[i - 1] + each));
};