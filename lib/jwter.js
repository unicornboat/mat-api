const config = require('../config/default');
const jwt = require('jsonwebtoken');
const language = require('./language');
const jwtToken = require('../app/models/jwtToken');

module.exports = function (req, res, next) {
	const auth_header = req.headers.authorization;

	if (auth_header) {
		const token = auth_header.split(' ')[1];
		jwt.verify(token, config.jwtSecret, (err, data) => {
			if (err || typeof data.id !== 'number') {
				res.status(403);
				res.send({
					error: 1,
					messages: [language('Invalid authorisation, please login first') + '[1]']
				});
				return;
			}
			jwtToken.isValid(token)
				.then(jwt_data => {
					if (jwt_data.user_id && data.id == jwt_data.user_id) {
						req.token = token;
						req.userId = data.id;
						next();
					} else {
						res.status(403);
						res.send({
							error: 1,
							messages: [language('Invalid authorisation, please login first') + '[2]']
						});
					}
				})
				.catch(err => {
					console.log(err);
					res.send({
						error: 1,
						messages: err.message
					});
				});
		});
		return;
	}

	res.status(401);
	res.send({
		error: 1,
		messages: [language('Invalid authorisation, please login first') + '[3]']
	});
}