const config = require('./config/default');
const cors = require('cors');
const path = require('path');
const express = require('express');
const routers = {
	admin: require('./app/routers/admin'),
	auth: require('./app/routers/auth'),
	message: require('./app/routers/message'),
	project: require('./app/routers/project'),
	search: require('./app/routers/search'),
	payment: require('./app/routers/payment'),
	upload: require('./app/routers/upload'),
	user: require('./app/routers/user')
};

global.APP_ROOT = path.resolve(__dirname);

// const jwtValidator = require('./lib/jwt-validator');
const language = require('./lib/language');
const app = express();

app.use(cors());
app.use('/public', express.static(path.join(__dirname, '/public')));

app.use(`/${config.apiPrefix}/v${config.apiVersion}/admin`, routers.admin);
app.use(`/${config.apiPrefix}/v${config.apiVersion}/auth`, routers.auth);
app.use(`/${config.apiPrefix}/v${config.apiVersion}/message`, routers.message);
app.use(`/${config.apiPrefix}/v${config.apiVersion}/project`, routers.project);
app.use(`/${config.apiPrefix}/v${config.apiVersion}/search`, routers.search);
app.use(`/${config.apiPrefix}/v${config.apiVersion}/payment`, routers.payment);
app.use(`/${config.apiPrefix}/v${config.apiVersion}/upload`, routers.upload);
app.use(`/${config.apiPrefix}/v${config.apiVersion}/user`, routers.user);

// app.get('/', (req, res) => {
// 	res.send(language('Matchkun API service v%s', config.version));
// })

app.listen(config.port, () => {
	console.log('Matchkun API server listening at port:%s', config.port);
})