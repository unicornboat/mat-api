# Matchkun API Server

## Deploy

### Server OD
Ubuntu 16

### Install NodeJs and NPM

```
sudo apt-get update

sudo apt-get install nodejs

sudo apt-get install npm
```

#### Install NVM to manage NodeJs and NPM

Always check the latest version on the [NVM official repo](https://github.com/nvm-sh/nvm). Normally install the latest LTS version.

```
cd ~
wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
source ~/.profile
nvm ls-remote
```

Example:
```
nvm install 14.15.4
nvm alias default 14.15.4
nvm use default
```

## Fix `npm install` issue

```
sudo npm install -g n
sudo n latest
sudo npm install -g npm
hash -d npm
npm i
```